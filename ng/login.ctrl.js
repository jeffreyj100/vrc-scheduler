(function() {
	'use strict';
	angular.module('app')
	.controller('LoginCtrl', function ($scope, $rootScope, UserSvc, $location, $window, $uibModal) {
		if($scope.currentUser) { $location.path('/schedule') } // Redirect if user already logged in

		$scope.login = function (username, password) {
			UserSvc.login(username, password)
			.then(function (user) {
				$scope.$emit('login', user);
				$location.path('/schedule');
			});
		};

		// View password reset modal
		$scope.viewResetPassword = function() {
			$uibModal.open({
				templateUrl: 'modalResetPassword.html',
				scope: $scope
			});
		};

		$scope.initResetPassword = function(emailaddress) {
			var username,
				found = false;

			UserSvc.getMembersInfo() // Find members username to send with email
			.then(function(membersInfo) {
				for(var i = 0; i < membersInfo.length; i++) {
					if(membersInfo[i].emailaddress === emailaddress) {
						found = true;
						username = membersInfo[i].username;

						UserSvc.resetPassword(username, emailaddress)
						.then(function(result) {
							if(result.status === 200) {
								alert("Reset instructions sent to your email.");
								$window.location.reload();
							}
						});
					}
				}

				if(!found) { // Bluff for security sake
					alert("Reset instructions sent to your email.");
					$window.location.reload();
				}
			});
		};
	});
}());

