(function() {
    'use strict';
    angular.module('app')
    .service('PostsSvc', function($http) {
        this.fetch = function() {
            return $http.get('/api/posts')
            .then(function(response) {
               return response.data;
            });
        };

        this.create = function(firstname, lastname, post) {
          return $http.post('/api/posts', {
              firstname: firstname,
              lastname: lastname,
              post: post
          });
        };
    });
}());