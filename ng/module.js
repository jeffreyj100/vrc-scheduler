(function() {
	'use strict';
	angular.module('app', [
		'ngRoute',
		'mwl.calendar',	// Angular Bootstrap Calendar
		'ui.bootstrap',
		'ui.utils' // Angular utils for phone format mask
	]);
}());

