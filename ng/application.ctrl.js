(function() {
	'use strict';
	angular.module('app')
	.controller('ApplicationCtrl', function ($scope, $http, $location, $q, UserSvc) {
		$scope.currentUser = false;

		// Persist user in page refresh if not logged out
		function rememberUser() {
			if(window.localStorage.token) {
				$http.defaults.headers.common['X-Auth'] = window.localStorage.token; // Prepare to send token
				UserSvc.getUser()
				.then(function (user) {
					if(!user) { $location.path('/login') } // Redirect if user not logged in
					$scope.currentUser = user;
				});
			} else {
				$location.path('/login'); // Redirect if user not logged in
			}
		};

		rememberUser();

		$scope.$on('login', function (_, user) {
			$scope.currentUser = user;
		});

		$scope.logout = function() {
			delete $scope.currentUser;
			delete window.localStorage.token;
		};
	});
}());
