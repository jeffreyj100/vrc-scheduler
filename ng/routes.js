(function() {
	'use strict';
	angular.module('app')
	.config(function ($routeProvider, $locationProvider, $httpProvider) {
		$routeProvider
		.when('/', { controller: 'LoginCtrl', templateUrl: '/templates/login.html' })
		.when('/login', { controller: 'LoginCtrl', templateUrl: '/templates/login.html' })
		.when('/register', { controller: 'RegisterCtrl', templateUrl: '/templates/register.html' })
		.when('/schedule', { controller: 'WorkBlockCtrl', templateUrl: '/templates/schedule.html'})
		.when('/othercampus', { controller: 'WorkBlockCtrl', templateUrl: '/templates/othercampus.html' })
		.when('/discussion', { controller: 'PostsCtrl', templateUrl: '/templates/discussion.html' })
		.when('/settings', { controller: 'SettingsCtrl', templateUrl: '/templates/settings.html'})
		.otherwise({ redirectTo: '/login'});

		$locationProvider.html5Mode({enabled: true, requireBase: true});
		$httpProvider.interceptors.push('responseInterceptor');
	})
	.factory('responseInterceptor', function($q, $location) {
		return {
			responseError: function(rejection) {
				if(rejection.status === 401) {
					if($location.path() === '/login') { alert("Invalid username and/or password.") }
					if($location.path() === '/settings') { alert("Current password is invalid.") }
					if($location.path() === '/register') { alert("Invalid registration code.") }
				}
				return $q.reject(rejection);
			}
		}
	});
}());


