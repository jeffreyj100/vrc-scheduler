(function() {
	'use strict';
	angular.module('app')
	.controller('RegisterCtrl', function ($scope, UserSvc, $location) {
		if($scope.currentUser) { $location.path('/schedule') } // Redirect if user already logged in

		$scope.register = function (firstname, lastname, campus, phonenumber, emailaddress, username, password, regCode) {
			UserSvc.register(firstname, lastname, campus, phonenumber, emailaddress, username, password, regCode)
			.then(function (user) {
				$scope.$emit('login', user);
				$location.path('/schedule');
			});
		};
	});
}());

