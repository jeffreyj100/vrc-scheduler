(function() {
	'use strict';
	angular.module('app')
	.service('UserSvc', function ($http, $q) {
		var svc = this;

		// Format phone number to (xxx) xxx-xxxx
		var formatPhoneNum = function(phoneNum) {
			var origNum = phoneNum.toString().trim().replace(/^\+/, '');
			var country, city, number;

			if(origNum.match(/[^0-9]/)) { return phoneNum }

			switch (origNum.length) {
				case 10: // +1PPP####### -> C (PPP) ###-####
					country = 1;
					city = origNum.slice(0, 3);
					number = origNum.slice(3);
					break;
				case 11: // +CPPP####### -> CCC (PP) ###-####
					country = origNum[0];
					city = origNum.slice(1, 4);
					number = origNum.slice(4);
					break;
				case 12: // +CCCPP####### -> CCC (PP) ###-####
					country = origNum.slice(0, 3);
					city = origNum.slice(3, 5);
					number = origNum.slice(5);
					break;
				default:
					return phoneNum;
			}

			if (country == 1) { country = "" }

			number = number.slice(0, 3) + '-' + number.slice(3);
			return (country + " (" + city + ") " + number).trim();
		};

		// Get user
		svc.getUser = function () {
			return $http.get('/api/users')
			.then(function (response) {
				return response.data;
			});
		};

		// Log user in
		svc.login = function (username, password) {
			return $http.post('/api/sessions', {
				username: username, password: password
			})
			.then(function (response) {
				window.localStorage.token = response.data; // Save token to local storage
				$http.defaults.headers.common['X-Auth'] = response.data;
				return svc.getUser();
			});
		};

		// Register new user
		svc.register = function (firstname, lastname, campus, phonenumber, emailaddress, username, password, regCode) {
			var phonenumber = formatPhoneNum(phonenumber);

			return $http.post('/api/users', {
				firstname: firstname,
				lastname: lastname,
				campus: campus,
				phonenumber: phonenumber,
				emailaddress: emailaddress,
				username: username,
				password: password,
				regCode: regCode
			})
			.then(function () {
				return svc.login(username, password);
			});
		};

		// Get all members contact info from db
		svc.getMembersInfo = function() {
			return $http.get('/api/usersInfo')
			.then(function(response) {
				return response.data;
			});
		};

		// Update users account info
		svc.updateMemberFirstName = function(id, username, firstname) {
			var updateUser =  $http.put('/api/updateFirstName', { id: id, firstname: firstname }),
				updateBlock = $http.put('/api/updateBlockFirstName', { username: username, firstname: firstname });

			return $q.all([updateUser, updateBlock])
			.then(function(resultsArray) {
				return resultsArray;
			});
		};
		svc.updateMemberLastName = function(id, username, lastname) {
			var updateUser =  $http.put('/api/updateLastName', { id: id, lastname: lastname }),
				updateBlock = $http.put('/api/updateBlockLastName', { username: username, lastname: lastname });

			return $q.all([updateUser, updateBlock])
			.then(function(resultsArray) {
				return resultsArray;
			});
		};
		svc.updateMemberCampus = function(id, campus) {
			return $http.put('/api/updateCampus', {
				id: id,
				campus: campus
			})
			.then(function(response) {
				return response;
			});
		};
		svc.updateMemberPhoneNumber = function(id, phonenumber) {
			var phonenumber = formatPhoneNum(phonenumber);

			return $http.put('/api/updatePhoneNumber', {
				id: id,
				phonenumber: phonenumber
			})
			.then(function(response) {
				return response;
			});
		};
		svc.updateMemberEmailAddress = function(id, emailaddress) {
			return $http.put('/api/updateEmailAddress', {
				id: id,
				emailaddress: emailaddress
			})
			.then(function(response) {
				return response;
			});
		};

		// Change password
		svc.changePassword = function(id, username, currPassword, newPassword1) {
			return $http.post('/api/sessions', {
				username: username, password: currPassword
			})
			.then(function (response) {
				if(response) {
					return $http.put('/api/changePassword', {
						id: id,
						newPassword1: newPassword1
					});
				}
			});
		};

		// Forgot password so reset
		svc.resetPassword = function(username, emailaddress) {
			return $http.put('/api/resetPassword', {
				username: username,
				emailaddress: emailaddress
			})
			.then(function(response) {
				return response;
			});
		};

		// Delete account
		svc.deleteAccount = function(id, username) {
			var deleteBlocks = $http.put('/api/deleteAllUsersBlocks', { username: username }),
				deleteAccount = $http.put('/api/deleteAccount', { id: id });

			return $q.all([deleteBlocks, deleteAccount])
			.then(function(resultsArray) {
				return resultsArray;
			});
		};
	});
}());

