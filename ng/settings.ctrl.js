(function() {
    'use strict';
    angular.module('app')
    .controller('SettingsCtrl', function ($scope, $rootScope, $uibModal, UserSvc, $route, $location, $window) {
        // View account info modals
        $scope.viewUpdateFirstName = function() {
            $uibModal.open({
                templateUrl: 'modalUpdateFirstName.html',
                scope: $scope
            });
        };
        $scope.viewUpdateLastName = function() {
            $uibModal.open({
                templateUrl: 'modalUpdateLastName.html',
                scope: $scope
            });
        }
        $scope.viewUpdateCampus = function() {
            $uibModal.open({
                templateUrl: 'modalUpdateCampus.html',
                scope: $scope
            });
        };
        $scope.viewUpdatePhoneNumber = function() {
            $uibModal.open({
                templateUrl: 'modalUpdatePhoneNumber.html',
                scope: $scope
            });
        };
        $scope.viewUpdateEmailAddress = function() {
            $uibModal.open({
                templateUrl: 'modalUpdateEmailAddress.html',
                scope: $scope
            });
        };

        var id = $scope.currentUser["_id"]; // Update user using db id value
        var username = $scope.currentUser["username"]; // Update workblock with username value

        // Update Account info
        $scope.updateFirstName = function(firstname) {
            UserSvc.updateMemberFirstName(id, username, firstname)
            .then(function(response) {
                if(response[0].status === 200 && response[1].status === 200) {
                    $window.location.reload();
                } else {
                    //console.log("(test) responses !== 200");
                }
            });
        };
        $scope.updateLastName = function(lastname) {
            UserSvc.updateMemberLastName(id, username, lastname)
            .then(function(response) {
                if(response[0].status === 200 && response[1].status === 200) {
                    $window.location.reload();
                } else {
                    //console.log("(test) response !== 200");
                }
            });
        };
        $scope.updateCampus = function(campus) {
            UserSvc.updateMemberCampus(id, campus)
            .then(function(response) {
                if(response.status === 200) {
                    $window.location.reload();
                } else {
                    //console.log("(test) response !== 200");
                }
            });
        };
        $scope.updatePhoneNumber = function(phonenumber) {
            UserSvc.updateMemberPhoneNumber(id, phonenumber)
            .then(function(response) {
                if(response.status === 200) {
                    $window.location.reload();
                } else {
                    //console.log("(test) response !== 200");
                }
            });
        };
        $scope.updateEmailAddress = function(emailaddress) {
            UserSvc.updateMemberEmailAddress(id, emailaddress)
            .then(function(response) {
                if(response.status === 200) {
                    $window.location.reload();
                } else {
                    //console.log("(test) response !== 200");
                }
            });
        };

        // View change password modal
        $scope.viewChangePassword = function() {
            $uibModal.open({
                templateUrl: 'modalChangePassword.html',
                scope: $scope
            });
        };

        // Change password
        $scope.initChangePassword = function(currPassword, newPassword1, newPassword2) {
            if(newPassword1 !== newPassword2) { // Passwords don't match
                $scope.pwNoMatch = true;
                return false;
            } else { // Passwords match so continue
                $scope.pwNoMatch = false;
                UserSvc.changePassword(id, username, currPassword, newPassword1)
                .then(function(response) {
                    if(response.status === 200) {
                        $window.location.reload();
                    }
                });
            }
        };

        // View delete account modal
        $scope.viewDeleteAccount = function() {
            $uibModal.open({
                templateUrl: 'modalDeleteAccount.html',
                scope: $scope
            });
        };

        // Delete account
        $scope.initDeleteAccount = function() {
            UserSvc.deleteAccount(id, username)
            .then(function(response) {
                if(response[0].status === 200 && response[1].status === 200) {
                    delete $scope.currentUser;
                    delete window.localStorage.token;
                    $window.location.reload();
                    $location.path('/login');
                }
            });
        };
    });
}());

