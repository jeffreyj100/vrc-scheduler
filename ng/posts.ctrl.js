(function() {
    'use strict';
    angular.module('app')
    .controller('PostsCtrl', function($scope, PostsSvc) {
        $scope.addPost = function() {
            var firstname = $scope.currentUser["firstname"],
                lastname = $scope.currentUser["lastname"],
                body = $scope.postBody;

            if($scope.postBody) {
                PostsSvc.create(firstname, lastname, body)
                .then(function() {
                    $scope.postBody = null;
                });
            }
        };

        $scope.$on('ws:new_post', function(_, post) { // Websockets
           $scope.$apply(function() {
              $scope.posts.unshift(post);
           });
        });

        PostsSvc.fetch()
        .then(function(posts) {
            $scope.posts = posts;
        });
    });
}());