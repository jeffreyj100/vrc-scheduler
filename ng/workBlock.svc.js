(function() {
	'use strict';
	angular.module('app')
	.service('WorkBlockSvc', function($http) {
		this.fetch = function() {
			return $http.get('/api/workblocks')
			.then(function(response) {
				return response.data;
			});
		};

		this.create = function(workblock) {
			return $http.post('/api/workblocks', workblock);
		};

		this.deleteWorkBlock = function(currentUsername, startsAt) {
			return $http.put('/api/deleteWorkblock', {
				currentUsername: currentUsername,
				startsAt: startsAt
			})
			.then(function(response) {
				return response;
			});
		};
	});
}());
