(function() {
	'use strict';
	angular.module('app')
	.controller('WorkBlockCtrl', function($scope, WorkBlockSvc, UserSvc, $uibModal, moment, $location, $window) {
		// ---------- (Workblocks) ----------
		if($scope.currentUser) {
			var userCampus = JSON.stringify($scope.currentUser["campus"]); // Get campus
			$scope.campusLoc = userCampus;
			userCampus = userCampus.replace(/\"/g, ""); // Remove double quotes
			var differentCampus;
		}

		// Determine which campus to view
		if($location.path() === "/schedule") {
			differentCampus = false;
		} else if($location.path() === "/othercampus") {
			differentCampus = true;
			// Toggle campuses according to users own campus
			if(userCampus === "Colorado Springs") {
				$scope.othercampus = "Lowell";
			} else if(userCampus === "Lowell") {
				$scope.othercampus = "Colorado Springs";
			}
		}

		// Initialize dates
		$scope.today = function() {
			$scope.startAt = new Date();
			$scope.endAt = new Date();
		};
		$scope.today();
		// Initialize events array
		$scope.events = [];

		$scope.addWorkBlock = function() {
			if($scope.startsAt && $scope.endsAt) {
				// Convert time
				var startVal = moment.tz($scope.startsAt, "America/Denver").toDate(),
					endVal = moment.tz($scope.endsAt, "America/Denver").toDate();
				// Get Hours
				var startHour = startVal.getHours() % 12 || 12,
					endHour = endVal.getHours() % 12 || 12;
				// Get name
				var username = JSON.stringify($scope.currentUser["username"]),
					firstName = JSON.stringify($scope.currentUser["firstname"]),
					lastName = JSON.stringify($scope.currentUser["lastname"]);

				// Remove double quotes
				username = username.replace(/\"/g, "");
				firstName = firstName.replace(/\"/g, "");
				lastName = lastName.replace(/\"/g, "");

				$scope.events.push(
					{
						username: username,
						title: firstName + " " + lastName + ": " + startHour + " - " + endHour,
						startsAt: startVal,
						endsAt: endVal,
						draggable: false,
						resizable: false
					}
				);

				WorkBlockSvc.create(
					{
						firstname: firstName,
						lastname: lastName,
						campus: userCampus,
						starttime: $scope.startsAt,
						endtime: $scope.endsAt
					}
				)
				.then(function() {
					$scope.startAt = null;
					$scope.endAt = null;
				});
			}
		};

		$scope.currentUsername = $scope.currentUser["username"];
		var currentUsername = $scope.currentUser["username"];

		// Delete workblock
		$scope.initDeleteWorkBlock = function(event) {
			var startsAt = event["startsAt"];
			WorkBlockSvc.deleteWorkBlock(currentUsername, startsAt)
			.then(function(response) {
				if(response.status === 200) {
					$window.location.reload();
				}
			});
		};

		// Update data via websockets
		$scope.$on('ws:new_workblock', function(_, workblock) {
			$scope.$apply(function() {
				// Convert time
				workblock.starttime = moment.tz(workblock.starttime, "America/Denver").format();
				workblock.endtime = moment.tz(workblock.endtime, "America/Denver").format();
				$scope.workblocks.unshift(workblock);
			});
		});

		// Fetch data from db
		WorkBlockSvc.fetch()
		.then(function(workblocks) {
			if(workblocks.length > 0 && $scope.currentUser) {
				// Determine which campus to fetch
				differentCampus ? userCampus = $scope.othercampus : userCampus = userCampus; // Toggle campuses
				for(var i = 0; i < workblocks.length; i++) {
					// Get username
					var username = workblocks[i].username;
					// Convert time
					workblocks[i].starttime = moment.tz(workblocks[i].starttime, "America/Denver").format();
					workblocks[i].endtime = moment.tz(workblocks[i].endtime, "America/Denver").format();

					// Convert time
					var startVal = moment.tz(workblocks[i].starttime, "America/Denver").toDate(),
						endVal = moment.tz(workblocks[i].endtime, "America/Denver").toDate();
					// Get Hours
					var startHour = startVal.getHours() % 12 || 12,
						endHour = endVal.getHours() % 12 || 12;

					if(userCampus === workblocks[i].campus) { // If members are in same campus as currentUser
						$scope.events.push(
							{
								username: username,
								title: workblocks[i].firstname + " " + workblocks[i].lastname + ": " + startHour + " - " + endHour,
								startsAt: startVal,
								endsAt: endVal,
								draggable: false,
								resizable: false
							}
						);
					}
				}
			}

			$scope.workblocks = workblocks;
		});

		// ---------- (Members Info) ----------
		UserSvc.getMembersInfo()
		.then(function(membersInfo) {
			$scope.members = [];
			if(membersInfo.length > 0) {
				differentCampus ? userCampus = $scope.othercampus : userCampus = userCampus; // Toggle campuses
				for (var i = 0; i < membersInfo.length; i++) {
					// Always post supervisor info first
					if(membersInfo[i].supervisor === true) { $scope.members.push(membersInfo[i]) }
					// If members are in same campus as currentUser
					if (userCampus === membersInfo[i].campus && membersInfo[i].supervisor === false ) {
						$scope.members.push(membersInfo[i]);
					}
				}
			}
		});

		// View members info modal
		$scope.viewMembers = function() {
			$uibModal.open({
				templateUrl: 'modalMembers.html',
				scope: $scope
			});
		};

		// ---------- (Angular Bootstrap Calendar) ----------
		$scope.calendarView = 'week';
		$scope.calendarDay = new Date();
		$scope.isCellOpen = true;

		// Modal for each event
		function showModal(action, event) {
			$uibModal.open({
				templateUrl: 'modalEvent.html',
				scope: $scope,
				controller: function() {
					var vm = this;
					vm.action = action;
					vm.event = event;
				},
				controllerAs: 'vm'
			});
		}

		// Toggle calendar picker button
		$scope.openStart = function($event) {
			$scope.openedStart = true;
		};
		$scope.openEnd = function($event) {
			$scope.openedEnd = true;
		};

		$scope.eventClicked = function(event) {
			showModal('Clicked', event);
		};
	});
}());


