(function() {
	'use strict';
	var mongoose = require('mongoose');
	mongoose.connect('mongodb://localhost/vrcschedule', function () {
		console.log('vrcschedule mongodb connected');
	});

	module.exports = mongoose;
}());
