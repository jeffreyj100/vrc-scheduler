(function() {
	'use strict';
	var gulp = require('gulp');
	var stylus = require('gulp-stylus');

	gulp.task('css', function () {
		return gulp.src(['css/app.styl', 'css/*.styl', 'css/*.css'])
			.pipe(stylus())
			.pipe(gulp.dest('assets'));
	});

	gulp.task('watch:css', ['css'], function () {
		gulp.watch('css/**/*.styl', ['css']);
	});
}());

