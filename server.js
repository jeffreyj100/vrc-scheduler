(function() {
	'use strict';
	if(process.env.NODE_ENV === "development") { // Development
		console.log("(TEST) NODE_ENV = development");

		// https
		(function() {
			'use strict';
			var fs = require('fs');
			var https = require('https');
			var express = require('express');

			var bodyParser = require('body-parser');
			var logger = require('morgan');
			var websockets = require('./websockets');

			var app = express();
			app.use(bodyParser.json());
			app.use(logger('dev'));

			app.use(require('./auth'));
			app.use(require('./controllers'));

			var options = {
				key: fs.readFileSync('sslConfig/dev/server.key'),
				cert: fs.readFileSync('sslConfig/dev/server.crt')
			};

			var port = process.env.PORT || 3000;
			var server = https.createServer(options, app).listen(port, function () {
				console.log('Server', process.pid, 'listening on', port);
			});

			websockets.connect(server);
		}());
	} else { // Production
		console.log("(TEST) NODE_ENV = production");

		// https
		(function() {
			'use strict';
			var fs = require('fs');
			var http = require('http');
			var https = require('https');
			var express = require('express');

			var bodyParser = require('body-parser');
			var logger = require('morgan');
			var websockets = require('./websockets');

			var app = express();
			app.use(bodyParser.json());
			app.use(logger('dev'));

			app.use(require('./auth'));
			app.use(require('./controllers'));

			var options = {
				key: fs.readFileSync('sslConfig/prod/server.key'),
				cert: fs.readFileSync('sslConfig/prod/regisvrcscheduler_com.crt')
			};

			var httpsServer = https.createServer(options, app).listen(443, function () {
				console.log('Server', process.pid, 'listening on', 443);
			});

			// Redirect from http to https if needed
			http.createServer(function(req, res) {
				res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
				res.end();
			}).listen(80);

			//websockets.connect(httpsServer);
			websockets.connect(httpsServer);
		}());
	}
}());