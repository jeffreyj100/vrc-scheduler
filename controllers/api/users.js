(function() {
	'use strict';
	var router = require('express').Router();
	var bcrypt = require('bcryptjs');
	var jwt = require('jwt-simple');
	var pwGenerator = require('password-generator');
	var nodemailer = require('nodemailer');
	var sgTransport = require('nodemailer-sendgrid-transport');
	var config = require('../../config');
	var moongoose = require('mongoose');
	var User = require('../../models/user');

	// Sendgrid account options
	var options = {
		auth: { api_key: config.sendGridApiKey }
	}
	var client = nodemailer.createTransport(sgTransport(options));

	// Get authenticated user
	router.get('/users', function (req, res, next) {
		if (!req.headers['x-auth']) {
			return res.sendStatus(401);
		}
		var auth = jwt.decode(req.headers['x-auth'], config.secret);
		User.findOne({username: auth.username}, function (err, user) {
			if (err) { return next(err) }
			res.json(user);
		});
	});

	// Post newly registered user
	router.post('/users', function (req, res, next) {
		var regCode = req.body.regCode;

		if(config.regCode === regCode) { // Register as regular user
			var user = new User({
				firstname: req.body.firstname,
				lastname: req.body.lastname,
				campus: req.body.campus,
				phonenumber: req.body.phonenumber,
				emailaddress: req.body.emailaddress,
				username: req.body.username,
				supervisor: false
			});
		} else if(config.supRegCode === regCode) { // Register as supervisor
			var user = new User({
				firstname: req.body.firstname,
				lastname: req.body.lastname,
				campus: req.body.campus,
				phonenumber: req.body.phonenumber,
				emailaddress: req.body.emailaddress,
				username: req.body.username,
				supervisor: true
			});
		} else {
			res.sendStatus(401);
		}

		bcrypt.hash(req.body.password, 10, function (err, hash) {
			if (err) { return next(err) }
			user.password = hash;
			user.save(function (err) {
				if (err) { return next(err) }
				res.sendStatus(201);
			});
		});
	});

	// Get all users info
	router.get('/usersInfo', function(req, res, next) {
		User.find()
		.exec(function(err, membersInfo) {
			if(err) { return next(err) }
			res.json(membersInfo);
		});
	});

	// Update account info
	router.put('/updateFirstName', function (req, res, next) {
		var id = moongoose.Types.ObjectId(req.body.id),
			firstname = req.body.firstname;

		User.update({_id: id}, {$set: {firstname: firstname}}, function(err, data) {
			if(err) { return next(err) }
			res.json(data);
		});
	});
	router.put('/updateLastName', function (req, res, next) {
		var id = moongoose.Types.ObjectId(req.body.id),
			lastname = req.body.lastname;

		User.update({_id: id}, {$set: {lastname: lastname}}, function(err, data) {
			if(err) { return next(err) }
			res.json(data);
		});
	});
	router.put('/updateCampus', function (req, res, next) {
		var id = moongoose.Types.ObjectId(req.body.id),
			campus = req.body.campus;

		User.update({_id: id}, {$set: {campus: campus}}, function(err, data) {
			if(err) { return next(err) }
			res.json(data);
		});
	});
	router.put('/updatePhoneNumber', function (req, res, next) {
		var id = moongoose.Types.ObjectId(req.body.id),
			phonenumber = req.body.phonenumber;

		User.update({_id: id}, {$set: {phonenumber: phonenumber}}, function(err, data) {
			if(err) { return next(err) }
			res.json(data);
		});
	});
	router.put('/updateEmailAddress', function (req, res, next) {
		var id = moongoose.Types.ObjectId(req.body.id),
			emailaddress = req.body.emailaddress;

		User.update({_id: id}, {$set: {emailaddress: emailaddress}}, function(err, data) {
			if(err) { return next(err) }
			res.json(data);
		});
	});

	// Change password
	router.put('/changePassword', function (req, res, next) {
		var id = moongoose.Types.ObjectId(req.body.id),
			newPassword = req.body.newPassword1,
			hashedPassword;

		bcrypt.hash(newPassword, 10, function (err, hash) {
			if (err) { return next(err) }
			hashedPassword = hash;

			User.update({_id: id}, {$set: {password: hashedPassword}}, function(err, data) {
				if(err) { return next(err) }
				res.json(data);
			});
		});
	});

	// Forgot password so reset
	router.put('/resetPassword', function(req, res, next) {
		var username = req.body.username,
			emailaddress = req.body.emailaddress,
			resetPassword = pwGenerator(15, false), // Generate random password
			hashedPassword;

		bcrypt.hash(resetPassword, 10, function(err, hash) {
			if(err) { return next(err) }
			hashedPassword = hash;

			User.update({emailaddress: emailaddress}, {$set: {password: hashedPassword}}, function(err, data) {
				if(err) { // Can't find email account
					return next(err)
				} else { // Email account found, send email
					var email = { // Email content
						from: 'Do Not Reply <admin@regisvrc.com>',
						to: emailaddress,
						subject: 'Reset Password',
						text: "In order to reset your Regis VRC account use this new temporary password.  Once your logged in go to settings to create a new password.\r\nUsername: " + username + "\r\nTemp Password: " + resetPassword + "\r\nFrom your admin@regisvrc.com",
						html: "<p>In order to reset your Regis VRC account use this new temporary password.  Once your logged in go to settings to create a new password.</p><br><br>" + "Username: <b>" + username + "</b><br>Temp Password: <b>" + resetPassword + "</b><br><br>From your admin@regisvrc.com"
					};

					client.sendMail(email, function(err, info) { // Send email using sendgrid
						if(err) {
							console.log(err);
						} else {
							console.log("Message sent: " + JSON.stringify(info));
						}
					});
				}
				res.json(data);
			});
		})
	});

	// Delete account
	router.put('/deleteAccount', function(req, res, next) {
		var id = moongoose.Types.ObjectId(req.body.id);

		User.remove({_id: id}, function(err, result) {
			if(err) { return next(err) }
			res.json(result);
		});
	});

	module.exports = router;
}());

