(function() {
	'use strict';
	var router = require('express').Router();
	var websockets = require('../../websockets');
	var WorkBlock = require('../../models/workBlock');

	router.get('/workblocks', function(req, res, next) {
		WorkBlock.find()
		.exec(function(err, workblocks) {
			if(err) { return next(err) }
			res.json(workblocks);
		});
	});

	router.post('/workblocks', function(req, res, next) {
		var workblock = new WorkBlock({ firstname: req.body.firstname, lastname: req.body.lastname, campus: req.body.campus, starttime: req.body.starttime, endtime: req.body.endtime });
		workblock.username = req.auth.username;
		workblock.save(function(err, workblock) {
			if(err) { return next(err) }
			websockets.broadcast('new_workblock', workblock);
			res.status(201).json(workblock);
		});
	});

	// Also update blocks when first/last names changed
	router.put('/updateBlockFirstName', function(req, res, next) {
		var username = req.body.username,
			firstname = req.body.firstname;

		WorkBlock.update({username: username}, {$set: {firstname: firstname}}, function(err, data) {
			if(err) { return next(err) }
			res.json(data);
		});
	});
	router.put('/updateBlockLastName', function(req, res, next) {
		var username = req.body.username,
			lastname = req.body.lastname;

		WorkBlock.update({username: username}, {$set: {lastname: lastname}}, function(err, data) {
			if(err) { return next(err) }
			res.json(data);
		});
	});

	// Delete a specific workblock
	router.put('/deleteWorkblock', function(req, res, next) {
		var currentUsername = req.body.currentUsername,
			startsAt = req.body.startsAt;

		WorkBlock.remove({username: currentUsername, starttime: startsAt}, function(err, result) {
			if(err) { return next(err) }
			res.json(result);
		});
	});

	// Delete all users blocks
	router.put('/deleteAllUsersBlocks', function(req, res, next) {
		var username = req.body.username;

		WorkBlock.remove({username: username}, function(err, result) {
			if(err) { return next(err) }
			res.json(result);
		});
	});

	module.exports = router;
}());
