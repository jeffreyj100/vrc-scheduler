(function() {
	'use strict';
	var db = require('../db');
	var user = db.Schema({
		firstname: { type: String, required: true },
		lastname: { type: String, required: true },
		campus: { type: String, required: true },
		phonenumber: { type: String, required: true },
		emailaddress: { type: String, required: true },
		username: { type: String, required: true },
		password: { type: String, required: true, select: false },
		supervisor: { type: Boolean, required: true}
	});

	module.exports = db.model('User', user);
}());
