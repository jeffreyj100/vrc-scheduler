(function() {
	'use strict';
	var db = require('../db');
	var WorkBlock = db.model('WorkBlock', {
		username: {type: String, required: true },
		firstname: {type: String, required: true},
		lastname: {type: String, required: true},
		campus: {type: String, required: true},
		starttime: { type: Date, required: true },
		endtime: {type: Date, required: true }
	});

	module.exports = WorkBlock;
}());
