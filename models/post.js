(function() {
    'use strict';
    var db = require('../db');
    var Post = db.model('Post', {
        firstname: { type: String, required: true },
        lastname: { type: String, required: true },
        body: { type: String, required: true },
        date: { type: Date, required: true, default: Date.now }
    });

    module.exports = Post;
}());

