(function() {
	'use strict';
	var gulp = require('gulp');
	var fs = require('fs');
	var gulp = require('gulp');
	var mainBowerFiles = require('main-bower-files');
	var exists = require('path-exists').sync;

	fs.readdirSync(__dirname + '/gulp').forEach(function (task) {
		require('./gulp/' + task);
	});

	var bowerWithMin = mainBowerFiles().map( function(path, index, arr) {
		var newPath = path.replace(/.([^.]+)$/g, '.min.$1');
		return exists( newPath ) ? newPath : path;
	});

	gulp.task('bowerFiles', function() {
		return gulp.src(bowerWithMin)
			.pipe(gulp.dest('assets/bowerfiles'));
	});

	gulp.task('images', function () {
		return gulp.src(['img/*.png', 'img/*.jpg', 'img/*.ico'])
			.pipe(gulp.dest('assets/img'));
	});

	gulp.task('build', ['js', 'css']);
	gulp.task('watch', ['watch:js', 'watch:css']);
	gulp.task('dev', ['bowerFiles', 'images', 'watch', 'dev:server']);
}());

