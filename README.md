# Regis VRC Scheduler #

---------------

![regisvrclogin.png](https://bitbucket.org/repo/apoLb6/images/2161012656-regisvrclogin.png)

A scheduling app for Regis Universities Veterans Resource Center in order to streamline and automate the current manual process of creating a weekly work schedule of work study members using excel spreadsheets.  Since there are 2 main campuses each with their own set of work study members, it would be better to centralize this information.  Not only for work study members but for the supervisor.  

-------------

### Features ###

* Add/Remove/View work times on weekly calendar.
* View other members contact info.
* View other campus work schedule and contact info.

![regisvrcschedule.png](https://bitbucket.org/repo/apoLb6/images/2498260079-regisvrcschedule.png)

-------------------------

### Discussion Board ###

* Real time messaging between campuses.

![regisvrcdiscussion.png](https://bitbucket.org/repo/apoLb6/images/4058929536-regisvrcdiscussion.png)

------------------------

### Account Settings ###

* Allows member to update contact information, change password, or delete account.

-------------------

### Registration ####

* Member registers with their information specifics and uses a registration code provided by supervisor.
* Supervisor can register as normal and have their own supervisor registration code to use.

![regisvrcregistration.png](https://bitbucket.org/repo/apoLb6/images/1832540330-regisvrcregistration.png)

--------

### Built on MEAN stack ###

* MongoDB is used to store users, work blocks, and posts.
* Express is my npm package for building the web app.
* Angular is my client front-end framework for handling routing, models, and views.
* NodeJS is used for my backend RESTful API.

### Other tools used ###
* Bower is used to manage client side dependencies.
* Gulp is used to automate my build processes of minifying js/css, starting server, watching for file changes, and fetching minified bower assets.
* On server side, JSON Web Tokens (JWT) are used for authentication, bcrypt to hash passwords, web-sockets for real-time messaging, and nodemailer/sendgrid api to send password reset email.
* Protractor with chai is used for e2e testing.