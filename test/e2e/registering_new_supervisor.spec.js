var db = require('../../db');
var config = require('../../config');
var chai = require('chai');
chai.use(require('chai-as-promised'));
var expect = chai.expect;

describe('register new user', function() {
    it('clicks on register and submits form data', function() {
        // Go to homepage
        browser.get('https://localhost');

        // Click on registration button
        element(by.css('.register')).click();

        // Fill out and submit registration form
        element(by.model('firstname')).sendKeys('Bob');
        element(by.model('lastname')).sendKeys('Waldo');
        element(by.cssContainingText('option', 'Lowell')).click();
        element(by.model('phonenumber')).sendKeys('1112229898');
        element(by.model('emailaddress')).sendKeys('bwaldo@regis.edu');
        element(by.model('username')).sendKeys('bob100');
        element(by.model('password')).sendKeys('bob100password');
        element(by.model('regCode')).sendKeys(config.supRegCode);
        element(by.css('form .btn')).click();

        expect(element.all(by.css('.signInName')).getText()).to.eventually.contain("Signed in as  Bob Waldo (Supervisor)");
        //browser.pause();
    });
    afterEach(function() {
        // Logout
        element(by.css('.logout')).click();

        // Reset database as before
        db.connection.db.collection("users").remove({username: "bob100"}, function(err, result) {
            if(err) throw err;
            console.log(result);
        });
    });
});





