var db = require('../../db');
var chai = require('chai');
chai.use(require('chai-as-promised'));
var expect = chai.expect;

describe('making a post', function() {
    it('logs in and creates a new post', function() {
        // Go to homepage
        browser.get('https://localhost');

        // Fill out and submit login form
        element(by.model('username')).sendKeys('john100');
        element(by.model('password')).sendKeys('john100password');
        element(by.css('form .btn')).click();

        // Click on discussion button
        element(by.css('.discussion')).click();

        // Submit new post on discussion board page
        var post = "my new post" + Math.random(); // Random number surety
        element(by.model('postBody')).sendKeys(post);
        element(by.css('form .btn')).click();

        // Check if text is posted correctly
        expect(element.all(by.css('ul.list-group li')).first().getText()).to.eventually.contain(post);
        //browser.pause();
    });
    afterEach(function() {
        // Reset database as before
        db.connection.db.collection("posts").remove({firstname: "John", lastname: "Sweet"}, function(err, result) {
            if(err) throw err;
            console.log(result);
        });

        // Logout
        element(by.css('.logout')).click();
        //browser.pause();
    });
});





